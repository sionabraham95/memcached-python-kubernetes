import pytest
import time

from pymemcache.exceptions import MemcacheIllegalInputError
from memcached import TestMemcacheClient

test_data = (
    '',
    {'im_empty': ''},
    'PyTest',
    {'String': 'PyTest'},
    1,
    {'Integer': 1},
    1.0,
    {'Float': 1.0},
    [1, 2, 3, 4],
    {'List': [1, 2, 3, 4]},
    None,
    {'Null': None},
    ('py', 'test'),
    1j,
    False,
    b'Hello',
    bytearray(5),
    {"Name", "Fullname", "Email"},
    frozenset({"Name", "Fullname", "Email"}),
    range(6),
    ("Name", "Fullname")
)


def test_get_cached_data_no_expiry():
    tmc = TestMemcacheClient()

    test_number = 0
    for data in test_data:
        flag = tmc.set_cached_data(
            data=data,
            name=f"test_{test_number}"
        )

        data = tmc.get_cached_data(
            name=f"test_{test_number}",
        )

        assert flag
        assert data == data
        test_number += 1


def test_get_cached_data_after_expiry():
    tmc = TestMemcacheClient()

    test_number = 0
    for data in test_data:
        flag = tmc.set_cached_data(
            data=data,
            name=f"test_{test_number}",
            expire_time=1
        )

        time.sleep(2)

        data = tmc.get_cached_data(
            name=f"test_{test_number}",
        )

        assert flag
        assert not data
        test_number += 1


def test_get_cached_data_before_expiry():
    tmc = TestMemcacheClient()

    test_number = 0
    for data in test_data:
        flag = tmc.set_cached_data(
            data=data,
            name=f"test_{test_number}",
            expire_time=2
        )

        data = tmc.get_cached_data(
            name=f"test_{test_number}",
        )

        assert flag
        assert data == data
        test_number += 1


def test_get_cached_data_wrong_name():
    tmc = TestMemcacheClient()

    test_number = 0
    for data in test_data:
        flag = tmc.set_cached_data(
            data=data,
            name=f"test_{test_number}",
            expire_time=2
        )

        data = tmc.get_cached_data(
            name="test_",
        )

        assert flag
        assert not data
        test_number += 1


def test_get_cached_data_not_set():
    tmc = TestMemcacheClient()

    test_number = 0
    for data in test_data:

        data = tmc.get_cached_data(
            name="test_",
        )
        assert not data
        test_number += 1


def test_get_cached_data_0_expery():
    tmc = TestMemcacheClient()

    test_number = 0
    for data in test_data:
        with pytest.raises(ValueError):
            tmc.set_cached_data(
                data=data,
                name=f"test_{test_number}",
                expire_time=0
            )
            test_number += 1


def test_set_cached_name_none():
    tmc = TestMemcacheClient()

    with pytest.raises(TypeError):

        tmc.set_cached_data(
            data=test_data[1],
            name=None
        )


def test_set_cached_name_space():
    tmc = TestMemcacheClient()

    with pytest.raises(MemcacheIllegalInputError):

        tmc.set_cached_data(
            data=test_data[1],
            name='space name'
        )


def test_set_cached_key_same_name():
    tmc = TestMemcacheClient()
    tmc.set_cached_data(
        data=test_data[1],
        name='data_key'
    )

    tmc.set_cached_data(
        data=test_data[2],
        name='data_key'
    )

    data = tmc.get_cached_data(
        name="data_key",
    )
    assert data == test_data[1]
