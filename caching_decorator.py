import os
import pickle
import hashlib

from memcached import MemcacheClient 
from functools import wraps

MEMCACHED_HOST_STRING = os.getenv('MEMCACHED_HOST_STRING')

def store_in_cache(func, expire_time=7200):
    client = None
    if MEMCACHED_HOST_STRING:
        client = MemcacheClient(MEMCACHED_HOST_STRING)
    @wraps(func)
    def wrapper(*args, **kwargs):
        if client:
            key_to_hash = pickle.dumps([func.__name__, args, kwargs])
            key = str(hashlib.sha256(key_to_hash).hexdigest())
            print(key)
            data = client.get_cached_data(key)
            if data:
                return data
            else:
                result = func(*args, **kwargs)
                client.set_cached_data(result, key, expire_time=expire_time)
                return result
                
        else:
            return func(*args, **kwargs)
    return wrapper
